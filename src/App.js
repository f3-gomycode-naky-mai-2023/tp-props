import logo from './logo.svg';
import './App.css';
import Player from './Components/Player';

function App() {
  
  const joueur1 = {
    nom: "messi",
    url: "./image/messi.jpg",
    equipe: "barcelone",
    nationnalite:"argentin",
    numero: 9,
    age : 34 ,

  }
  
  const joueur2 = {
    nom: "mbappe",
    url: "./image/mbappe.jpg",
    equipe: "psg",
    nationnalite:"francaise",
    numero: 11,
    age : 25 ,

  }
  
  const joueur3 = {
    nom: "christano",
    url: "./image/christiano.jpg",
    equipe: "real",
    nationnalite:"portugais",
    numero: 7,
    age : 36 ,

  }
  
  const joueur4 = {
    nom: "benzema",
    url: "./image/benzema.jpg",
    equipe: "real",
    nationnalite:"francaise",
    numero: 10 ,
    age : 38 ,

  }
  return (
    <div className="App">
      <Player></Player>
    </div>
  );
}

export default App;
